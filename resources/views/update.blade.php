<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" type="text/css">

    <script src="{{ URL::asset('js/jquery-1.9.0.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/custom.js') }}" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.css') }}" type="text/css">
    <script src="{{ URL::asset('js/jquery.dataTables.js') }}" type="text/javascript"></script>
</head>
<body>
<div class="col-xs-12 col-md-6 col-md-push-3">
    <a class="btn btn-default" href="{{ URL::to('admin') }}">Quay lại</a><br>
    @if(session('messenger_success')) <span class="alert alert-success col-xs-12" style="float: left;">{{ session('messenger_success') }}</span>@endif
    @if(session('messenger_error')) <span class="alert alert-danger col-xs-12" style="float: left;">{{ session('messenger_error') }}</span>@endif
    <br>
    <form method="post" action="">
        {{ csrf_field() }}
        <table id="update_user">
            <tr style="display: none;">
                <td>Id</td>
                <td><input type="text" name="userId" value="{{ $user->id }}"></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" class="form-control" value="{{ $user->email }}" disabled="disabled"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password" class="form-control"></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><input type="text" name="name" class="form-control" value="{{ $user->name }}" required></td>
            </tr>
            <tr>
                <td>Role</td>
                <td>
                    <ul>
                        @foreach($listRole as $key => $role)
                            <li>{{ $role }}: <input type="checkbox" name="rol[]" value="{{ $key }}"
                                @foreach($rolesUser as $roleUupdate)
                                    <?php if($key == $roleUupdate) echo 'checked'; else echo 'no';?>
                                @endforeach
                                ></li>
                        @endforeach
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Permission</td>
                <td>
                    view<input type="checkbox" name="view" value="view" <?php if($persUser['view']) echo 'checked';?> >|
                    |create<input type="checkbox" name="create" value="create" <?php if($persUser['create']) echo 'checked';?> >|
                    update<input type="checkbox" name="update" value="update" <?php if($persUser['update']) echo 'checked';?> >|
                    delete<input type="checkbox" name="delete" value="delete" <?php if($persUser['delete']) echo 'checked';?> >|
                </td>
                {{--<td>
                    <ul>

                        @foreach($persUser as $keypers => $pers)

                            <li>
                                {{ $keypers }}
                                <ul>
                                    @foreach($pers as $keyper => $per)

                                        <li>{{ $keyper }}: <input type="checkbox" name="{{ $keyper }}_{{ $keypers }}" value="{{ $keyper }}.{{ $keypers }}"></li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </td>--}}
            </tr>
            <tr>
                <td>Active</td>
                <td><input type="checkbox" name="active" <?php if($user->active == 1) echo 'checked="checked"';?>></td>
            </tr>
            <tr>
                <td><button class="btn bg-primary" type="submit">Update</button></td>
            </tr>
        </table>

    </form>
</div>
</body>
</html>