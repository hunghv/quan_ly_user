<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" type="text/css">

<form method="POST" action="/auth/login">
    {!! csrf_field() !!}
    <table>
        @if (count($errors) > 0)
            <tr class="alert alert-danger">
                <td colspan="2">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </td>
            </tr>
        @endif

        @if(session('error')) <span class="alert alert-danger col-xs-12" style="float: left;">{{ session('error') }}</span>@endif

        <tr>
            <td>Email</td>
            <td><input type="email" name="email" value="{{ old('email') }}" required></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" id="password" required></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="checkbox" name="remember">Remember Me</td>
        </tr>
        <tr>
            <td></td>
            <td><button type="submit">Login</button></td>
        </tr>
    </table>

</form>