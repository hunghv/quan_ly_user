<html>
<head>
    <title>Create Role</title>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" type="text/css">

    <script src="{{ URL::asset('js/jquery-1.9.0.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/custom.js') }}" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.css') }}" type="text/css">
    <script src="{{ URL::asset('js/jquery.dataTables.js') }}" type="text/javascript"></script>
</head>
<body>
<div class="col-xs-12 col-md-6 col-md-push-3">
    <a class="btn btn-default" href="{{ URL::to('admin') }}">Quay lại</a><br>
    @if(session('messenger_success')) <span class="alert alert-success col-xs-12" style="float: left;">{{ session('messenger_success') }}</span>@endif
    @if(session('messenger_error')) <span class="alert alert-danger col-xs-12" style="float: left;">{{ session('messenger_error') }}</span>@endif
    <br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="">
        {{ csrf_field() }}
        <table>
            <tr>
                <td>Name</td>
                <td><input type="text" name="name" class="form-control" value="{{ old('name') }}" required></td>
            </tr>
            <tr>
                <td>Slug</td>
                <td><input type="text" name="slug" class="form-control" value="{{ old('slug') }}" required></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><input type="text" name="description" class="form-control" value="{{ old('description') }}" required></td>
            </tr>
            <tr>
                <td>Permission</td>
                <td>
                    <select class="form-control" name="permission">
                        @foreach($listPermission as $permission)
                            <option value="{{ $permission->id }}">{{ $permission->name }}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td><button class="btn bg-primary" type="submit">Create</button></td>
            </tr>
        </table>

    </form>
</div>
</body>
</html>