<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" type="text/css">

    <script src="{{ URL::asset('js/jquery-1.9.0.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/custom.js') }}" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.css') }}" type="text/css">
    <script src="{{ URL::asset('js/jquery.dataTables.js') }}" type="text/javascript"></script>
</head>
<body>
<script type="text/javascript">
    $(document).ready(function() {
        $('#data_table').DataTable();
    } );
</script>

<div class="col-xs-12 col-md-4 left">
    <form action="" method="post">
        @if(session('messenger_success')) <span class="alert alert-success col-xs-12" style="float: left;">{{ session('messenger_success') }}</span>@endif
        @if(session('messenger_error')) <span class="alert alert-danger col-xs-12" style="float: left;">{{ session('messenger_error') }}</span>@endif
        {{ csrf_field() }}
        <table>
            <tr>
                <td>Email</td>
                <td><input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" class="form-control" name="password" placeholder="Password" value="{{ old('password') }}"></td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td><input type="password" class="form-control" name="repassword" placeholder="Password"></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}"></td>
            </tr>
            <tr>
                <td>Role</td>
                <td>
                    <ul>
                        <li>normal user <input type="checkbox" id="user" name="user" value="3"></li>
                        <li>Moderate <input type="checkbox" id="mod" name="mod" value="2"></li>
                        @if($userRoleId == 1)
                            <li>Admin <input type="checkbox" id="admin" name="admin" value="1"></li>
                        @endif
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Other Permission</td>
                <td>
                    view<input type="checkbox" name="per[]" value="view" id="view">|
                    |create<input type="checkbox" name="per[]" value="create" id="create">|
                    update<input type="checkbox" name="per[]" value="update" id="update">|
                    delete<input type="checkbox" name="per[]" value="delete" id="delete">|
                </td>
            </tr>
            <tr>
                <td>Active</td>
                <td><input type="checkbox" name="active" checked="checked"></td>
            </tr>
            <tr>
                <td></td>
                <td><button class="btn btn-primary" type="submit">Add</button></td>
            </tr>
        </table>
    </form>
</div>
<div class="col-xs-12 col-md-8 right">
    <a href="{{ URL::to('admin/create-role') }}" class="btn btn-success create-role">Create Role</a>
    <a href="{{ URL::to('auth/logout') }}" class="btn btn-danger logout">Logout</a>
    <table id="data_table" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>STT</th>
            <th>Email</th>
            <th>Name</th>
            <th>Role</th>
            <th>Active</th>
            <th>Create by</th>
            <th>Action</th>
            <th style="display:none;"></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>STT</th>
            <th>Email</th>
            <th>name</th>
            <th>Role</th>
            <th>Active</th>
            <th>Create by</th>
            <th>Action</th>
            <th style="display: none;"></th>
        </tr>
        </tfoot>
        <?php $colum_id = 1; ?>
        <tbody>
        @foreach($listUser as $user)
            <tr>
                <td>{{$colum_id}}</td>
                <td><a href="admin/update/<?php echo $user->id;?>">{{ $user->email }}</a></td>
                <td>{{ $user->name }}</td>
                <td><?php foreach($user->getRoles() as $key => $value){ echo $value . ' ';};?></td>
                <td><?php if($user->active == 1){echo 'Yes';} else echo 'No';?></td>
                <td><?php echo $user->create_by_user;?></td>
                <td><img id="img-del" src="{{ URL::asset('images/delete-button.png') }}"></td>
                <td id="id" style="display: none;">{{ $user->id }}</td>
            </tr>
            <?php $colum_id++ ; ?>
        @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $('#data_table #img-del').each(function(){
        $(this).click(function(){
            if(confirm('Are you sure delete this?')) {
                var tr = $(this).parents('tr:first');
                var id = $(tr).find('#id').html();

                $.ajax({
                    url: '{{ URL::to("admin/delete") }}/' + id,
                    type: 'GET',
                    contentType: 'application/json',
                    dataType: 'json',
                    success: function(resp) {
                        if(resp.status) {
                            $(tr).remove();
                        } else {
                            return alert(resp.message);
                        }
                    },
                    error: function(resp) {
                        alert('Faild!');
                    }
                });
            }
        });
    });

    $('#admin').click(function(){
        $('#view').prop( "checked" );
    });
</script>
</body>
</html>