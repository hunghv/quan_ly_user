<?php
namespace App\SessionProvider;

interface SessionInterface {

    public function open($savePath, $sessionName);

    public function close();

    public function read($sessionId);

    public function write($sessionId, $data);

    public function destroy($sessionId);

    public function gc($lifetime);
}