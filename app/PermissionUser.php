<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionUser extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
}
