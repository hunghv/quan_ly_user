<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*Route::get('create-role', 'RoleController@getIndex');
Route::get('create-permission', 'RoleController@createPermission');*/


Route::get('test', '\App\Http\Controllers\User\UserController@getIndex');

Route::get('profile', ['middleware' => 'auth', function() {
    return view('welcome');
}]);

Route::controller('test-service-provider', 'TestServiceProviderController');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('admin/create-role', 'AdminController@getCreateRole');
Route::controller('admin', 'AdminController');

Route::get('/', function () {
    if(!\Auth::check()){
        return redirect('/admin') ;
    }else{
        return view('welcome');
    }

});

//Route::get('/', ['middleware'   => ['auth', 'acl']]);