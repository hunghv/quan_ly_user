<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\PermissionUser;
use App\Role;
use App\Permission;
use App\RoleUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kodeine\Acl\Models\Eloquent\Permission as KodeineModel;
use Symfony\Component\HttpFoundation\Session\Flash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex(Request $request) {
        $user = \Auth::user() ;

        $users = User::get();

        $userRoles = $user->getRoles();

        $userRoleId = 3;
        foreach($userRoles as $key =>  $value) {
            if($key < $userRoleId) $userRoleId = $key;
        }

        $listUser = array();
        foreach($users as $member) {
            $userCreate = User::find($member->create_by);
            $member->create_by_user = $userCreate->name;
            $listUser[] = $member;
        }

        return view('admin')->with('listUser', $listUser)->with('userRoleId', $userRoleId);
    }

    public function postIndex(Request $request) {
        $userAction = \Auth::user();
        if($userAction->can('create.admin')) {
            if($request->password == '' || $request->password != $request->repassword) {
                $request->flash();
                return redirect('admin')->with('messenger_error', 'Password Error!')->withInput();
            }

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->active = $request->active ? 1 : 0;
            $user->create_by = $request->user()->id;
            if($user->save()) {
                $role_user = new RoleUser();

                if($request->user == null && $request->mod == null && $request->admin == null) {
                    $user->assignRole(3);
                } else {
                    if($request->user) $user->assignRole(3);
                    if($request->mod) $user->assignRole(2);
                    if($request->admin) $user->assignRole(1);

                    if($request->per != null) {
                        foreach($request->per as $key => $value) {
                            if($value == 'create') $user->addPermission('create.admin', true);
                            if($value == 'view')   $user->addPermission('view.admin', true);
                            if($value == 'update') $user->addPermission('update.admin', true);
                            if($value == 'delete') $user->addPermission('delete.admin', true);
                        }
                    }

                    return redirect('admin')->with('messenger_success', 'Success!');
                }
            } else {
                $request->flash();
                return redirect('admin')->with('messenger_error', 'Save User Error!')->withInput();
            }
        } else {
            return redirect('admin')->with('messenger_error', 'Không có quyền tạo!');
        }
    }

    public  function getUpdate($id, Request $request) {
        $user = \Auth::user();
        $userUpdate = User::find($id);

        $rolesUser = array();
        foreach($user->getRoles() as $key => $value) {
            $rolesUser[] = $key;
        };

        $roluse = $user->getRoles();
        $i = 3;
        foreach($roluse as $key => $value) {
            if($key < $i) {
                $i = $key;
            }
        }

        $roluserUpdate = $userUpdate->getRoles();
        $j = 3;
        foreach($roluserUpdate as $key => $value) {
            if($key < $j) {
                $j = $key;
            }
        }

        if($i >= $j) return 'Không có quyền Update!';

        if($user->can('update.admin') ) {
            $user = User::find($id);

            $persUser = array();
            $user->can('view.admin') ? $persUser['view'] = true : $persUser['view'] = false;
            $user->can('create.admin') ? $persUser['create'] = true : $persUser['create'] = false;
            $user->can('update.admin') ? $persUser['update'] = true : $persUser['update'] = false;
            $user->can('delete.admin') ? $persUser['delete'] = true : $persUser['delete'] = false;





            if($i == 1) {
                $listRole = array();
                $listRole[1] = 'administrator';
                $listRole[2] = 'moderate';
                $listRole[3] = 'normal user';
            } else {
                $listRole = array();
                $listRole[2] = 'moderate';
                $listRole[3] = 'normal user';
            }

            return view('update')->with('user', $user)->with('listRole', $listRole)->with('rolesUser', $rolesUser)
                ->with('persUser', $persUser);
        } else {
            return 'Không có quyền Update!';
        }
    }

    public function postUpdate(Request $request) {

        $user = User::find($request->userId);
        $user->name = $request->name;
        if($request->password) $user->password = bcrypt($request->password);
        $user->active = $request->active ? 1 : 0;
        if($user->save()) {

            if ($request->rol != null) {
                foreach ($request->rol as $key => $value) {
                    $user->assignRole($value);
                }
            }

            PermissionUser::where('user_id', '=', $user->id)->delete();
            if ($request->view) $user->addPermission('view.admin', true);
            if ($request->create) $user->addPermission('create.admin', true);
            if ($request->update) $user->addPermission('update.admin', true);
            if ($request->delete) $user->addPermission('delete.admin', true);

            return redirect('admin')->with('messenger_success', 'Success!');
        } else {
            $request->flash();
            return redirect('admin/update')->with('messenger_error', 'Save User Error!')->withInput();
        }
    }

    public function getDelete($id) {
        $user = \Auth::user();
        $userDelete = User::find($id);

        $rolesUser = array();
        foreach($user->getRoles() as $key => $value) {
            $rolesUser[] = $key;
        };

        $roluse = $user->getRoles();
        $i = 3;
        foreach($roluse as $key => $value) {
            if($key < $i) {
                $i = $key;
            }
        }

        $roluserUpdate = $userDelete->getRoles();
        $j = 3;
        foreach($roluserUpdate as $key => $value) {
            if($key < $j) {
                $j = $key;
            }
        }

        if($i >= $j) {
            $data = [
                'status'    => false,
                'messege'   => 'Không có quyền xóa user cấp cao hơn!'
            ];
        } else {
            if(is_numeric($id)) {
                if(RoleUser::where('user_id', '=', $id)->delete()) {
                    if(User::destroy($id)) {
                        $data = [
                            'status'    => true,
                            'message'   => 'Deleted!'
                        ];
                    } else {
                        $data = [
                            'status'    => false,
                            'messege'   => 'Error delete User!'
                        ];
                    }
                } else {
                    $data = [
                        'status'    => false,
                        'messege'   => 'Error delete Role User!'
                    ];
                };
            }  else {
                $data = [
                    'status'    => false,
                    'messege'   => 'Something has gone wrong!'
                ];
            }
        }

        return response()->json($data);
    }

    public function getCreateRole() {
        $listPermission = Permission::get();
        return view('createRole')->with('listPermission', $listPermission);
    }

    public function postCreateRole(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:name|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('createRole')
                ->withErrors($validator)
                ->withInput();
        }

        die('hung');
    }
}