<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Session;
use App\Role;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */


    use ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    protected $redirectHome = '/';

    protected $redirectAdmin = 'admin';

    protected $loginPath = 'auth/login';

    protected $redirectTo = '/';


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getLogin(){
        return view('auth.login');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:4',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function postLogin(LoginRequest $request)
    {
        $auth = array(
            'email' => $request->email,
            'password' => $request->password,
            'active' => 1
        );

        if(\Auth::attempt($auth)){
            $user = Auth::user();

            if($user->can('view.admin')) {
                return redirect('admin');

            } else {
                return redirect('/');
            }
        } else {
            Session::flash('error', 'Wrong!');
            return Redirect::to('auth/login')->withInput($request->except('password'));
        }
    }

    public function getLogout() {
        Auth::logout();
        return redirect('auth/login');
    }

    /*public function postLogin(LoginRequest $request)
    {
        $auth = array(
            'email' => $request->email,
            'password' => $request->password
        );
        // dd($auth);
        if(\Auth::attempt($auth)){
            $user = Auth::user();
            if($user->is('administrator')){
                return Redirect('user');
            }elseif($user->is('moderate')){
                return Redirect('user');
            }else
                return 'wellcome';
            //return \Redirect::action('HomeController@hello');
        }
        else{
            echo "Xin nhap lai email va password";
        }
    }*/
}
