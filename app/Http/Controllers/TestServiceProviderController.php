<?php

namespace App\Http\Controllers;

use App\SessionProvider\SessionEloquent;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class TestServiceProviderController extends Controller
{

    protected $_session;

    public function __construct(SessionEloquent $session)
    {
        $this->_session = $session;
    }

    public function getIndex()
    {
        $users = User::all();

        return view('test.serviceprovider')->with('users', $users);
    }

    public function postIndex(Request $request) {


        dd(Session::get(28));
        $uId = $request->uId;
        $param = array();
        $result = '';
        foreach($uId as $key => $value) {
            $param = User::find($value);
            if($this->_session->write($value, $param->name)) {
                $result = $result . 'Luu session cho user ' . $param->name . ' thanh cong!' . '<br>';
            } else {
                $result = $result . 'Luu session cho user ' . $param->name . ' that bai!' . '<br>';
            }
        }
        return $result;
    }

}