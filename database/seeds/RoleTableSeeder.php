<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Administrator',
            'slug' => 'administrator',
            'description' => 'administrator',
        ]);

        DB::table('roles')->insert([
            'name' => 'Moderate',
            'slug' => 'moderate',
            'description' => 'moderate',
        ]);

        DB::table('roles')->insert([
            'name' => 'Normal User',
            'slug' => 'normal user',
            'description' => 'normal user',
        ]);
    }
}