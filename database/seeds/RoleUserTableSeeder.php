<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$roles = Role::where('id', '>', 0)
            ->get();

        $roleId = array();
        foreach($roles as $role) {
            $roleId[] = $role->id;
        }
        shuffle($roleId);

        $userId = DB::table('users')->max('id');

        DB::table('role_user')->insert([
            'role_id' => $roleId[0],
            'user_id' => $userId,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);*/

        $user1 = User::find(1);
        $role1 = Role::find(1);
        $user1->assignRole($role1);

        $user2 = User::find(2);
        $role2 = Role::find(2);
        $user2->assignRole($role2);

        $user3 = User::find(3);
        $role3 = Role::find(3);
        $user3->assignRole($role3);
    }
}
