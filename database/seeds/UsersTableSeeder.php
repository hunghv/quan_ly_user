<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'create_by' => 1,
            'active' => 1
        ]);

        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'mod@gmail.com',
            'password' => bcrypt('secret'),
            'create_by' => 1,
            'active' => 1
        ]);

        DB::table('users')->insert([
            'name' => str_random(10),
            'email' => 'user@gmail.com',
            'password' => bcrypt('secret'),
            'create_by' => 1,
            'active' => 1
        ]);
    }
}