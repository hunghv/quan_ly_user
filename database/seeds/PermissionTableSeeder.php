<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'admin',
            'slug' => '{"create":true,"view":true,"update":true,"delete":true,"view.phone":true}',
            'description' => 'site admin',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Moderate',
            'slug' => '{"create":true,"view":true,"update":true,"delete":true,"view.phone":true}',
            'description' => 'manage Administrator permissions',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Normal User',
            'slug' => '{"create":false,"view":true,"update":false,"delete":false,"view.phone":true}',
            'description' => 'manage Normal User permissions',
        ]);
    }
}
